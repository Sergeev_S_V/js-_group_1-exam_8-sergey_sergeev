import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Header from "./Components/Header/Header";
import AddNewQuote from "./Containers/Quotes/AddNewQuote/AddNewQuote";
import MenuCategories from "./Components/MenuCategories/MenuCategories";
import AllQuotes from "./Containers/Quotes/AllQuotes/AllQuotes";
import QuotesStarWars from "./Containers/Quotes/Categories/StarWars/StarWars";
import FamousPeople from "./Containers/Quotes/Categories/FamousPeople/FamousPeople";
import Saying from "./Containers/Quotes/Categories/Saying/Saying";
import Humour from "./Containers/Quotes/Categories/Humour/Humour";
import Motivational from "./Containers/Quotes/Categories/Motivational/Motivational";
import EditQuote from "./Containers/Quotes/EditQuote/EditQuote";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route path='/' component={Header}/>
        <Route path='/' component={MenuCategories}/>
        <Switch>
          <Route path='/' exact component={AllQuotes}/>
          <Route path='/add-quote' exact component={AddNewQuote}/>
          <Route path='/quotes/:id/edit' exact component={EditQuote}/>
          <Route path='/quotes/star-wars' exact component={QuotesStarWars}/>
          <Route path='/quotes/famous-people' exact component={FamousPeople}/>
          <Route path='/quotes/saying' exact component={Saying}/>
          <Route path='/quotes/humour' exact component={Humour}/>
          <Route path='/quotes/motivational' exact component={Motivational}/>
        </Switch>
      </div>
    );
  }
}

export default App;
