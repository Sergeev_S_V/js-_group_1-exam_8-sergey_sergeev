import React, { Component } from 'react';
import './MenuCategories.css';

class MenuCategories extends Component {

  showAllQuotes = () => {
    this.props.history.push('/');
  };

  showStarWarsQuotes = () => {
    this.props.history.push('/quotes/star-wars');
  };

  showFamousPeopleQuotes = () => {
    this.props.history.push('/quotes/famous-people');
  };

  showSayingQuotes = () => {
    this.props.history.push('/quotes/saying');
  };

  showHumourQuotes = () => {
    this.props.history.push('/quotes/humour');
  };

  showMotivationalQuotes = () => {
    this.props.history.push('/quotes/motivational');
  };

  render() {
    return(
      <div className='MenuCategories'>
        <ul>
          <li onClick={this.showAllQuotes}>All</li>
          <li onClick={this.showStarWarsQuotes}>Star Wars</li>
          <li onClick={this.showFamousPeopleQuotes}>Famous people</li>
          <li onClick={this.showSayingQuotes}>Saying</li>
          <li onClick={this.showHumourQuotes}>Humour</li>
          <li onClick={this.showMotivationalQuotes}>Motivational</li>
        </ul>
      </div>
    );
  }
}

export default MenuCategories;