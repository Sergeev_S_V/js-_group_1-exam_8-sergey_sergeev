import React, {Component, Fragment} from 'react';
import './AllQuotes.css';
import axios from 'axios';
import Quote from "../../../Components/Quote/Quote";
import Spinner from "../../../Components/UI/Spinner/Spinner";

class AllQuotes extends Component {

  state = {
    quotes: [],
    loading: false,
  };

  getQuotesFromAPI = () => {
    this.setState({loading: true}, () => {
      axios.get('/quotes.json')
        .then(response => {
          if (response) {
            this.setState({quotes: response.data, loading: false})
          }
        })
    });
  };

  editQuote = key => {
    this.props.history.push(`/quotes/${key}/edit`);
  };

  deleteQuote = key => {
    this.setState({loading: true}, () => {
      axios.delete(`/quotes/${key}.json`)
        .then(response => {
          if (response) {
            this.getQuotesFromAPI();
          }
        })
    });
  };

  componentDidMount() {
    this.getQuotesFromAPI();
  };

  render() {
    let quotes = (
      <Fragment>
        {Object.keys(this.state.quotes).map(key => {
          return <Quote key={key}
                        author={this.state.quotes[key].author}
                        text={this.state.quotes[key].text}
                        edit={() => this.editQuote(key)}
                        delete={() => this.deleteQuote(key)}
          />
        })}
      </Fragment>
    );

    if (this.state.loading) {
      quotes = <Spinner/>
    }

    return(
      <div className='AllQuotes'>
        <h1 style={{color: '#9d9d9d'}}>All quotes</h1>
        {quotes}
      </div>
    );
  }
}

export default AllQuotes;