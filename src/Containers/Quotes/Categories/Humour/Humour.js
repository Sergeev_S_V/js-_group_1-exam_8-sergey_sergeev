import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Quote from "../../../../Components/Quote/Quote";
import Spinner from "../../../../Components/UI/Spinner/Spinner";
import './Humour.css';

class Humour extends Component {

  state = {
    quotes: [],
    loading: false,
  };

  getQuotesCurrentCategory = () => {
    axios.get('/quotes.json?orderBy="category"&equalTo="Humour"')
      .then(response => {
        if (response) {
          this.setState({quotes: response.data, loading: false})
        }
      })
  };

  editQuote = key => {
    this.props.history.push(`/quotes/${key}/edit`);
  };

  deleteQuote = key => {
    this.setState({loading: true}, () => {
      axios.delete(`/quotes/${key}.json`)
        .then(response => {
          if (response) {
            this.getQuotesCurrentCategory();
          }
        })
    });
  };

  componentDidMount() {
    this.setState({loading: true}, () => {
      this.getQuotesCurrentCategory();
    });
  };

  render() {
    let quotes = (
      <Fragment>
        {Object.keys(this.state.quotes).map(key => {
          return <Quote key={key}
                        author={this.state.quotes[key].author}
                        text={this.state.quotes[key].text}
                        edit={() => this.editQuote(key)}
                        delete={() => this.deleteQuote(key)}/>
        })}
      </Fragment>
    );

    if (this.state.loading) {
      quotes = <Spinner/>
    }

    return(
      <div className='Humour'>
        <h1 style={{color: '#9d9d9d'}}>Humour</h1>
        {quotes}
      </div>
    );
  }
}

export default Humour;