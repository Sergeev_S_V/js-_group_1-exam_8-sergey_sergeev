import React, {Component, Fragment} from 'react';
import './AddNewQuote.css';
import axios from 'axios';
import Button from "../../../Components/UI/Button/Button";
import Spinner from "../../../Components/UI/Spinner/Spinner";
import './AddNewQuote.css';

class AddNewQuote extends Component {

  state = {
    author: '',
    text: '',
    category: 'Star Wars',
    loading: false,
  };

  changeCategoryHandler = event => {
    this.setState({category: event.target.value});
  };

  changeAuthorHandler = event => {
    this.setState({author: event.target.value})
  };

  changeTextHandler = event => {
    this.setState({text: event.target.value})
  };

  saveQuote = () => {
    this.setState(() => {
      axios.post('/quotes.json', {
        category: this.state.category,
        author: this.state.author,
        text: this.state.text,
      }).then(() => {
          this.props.history.push('/');
        })
    });
  };

  render() {
    let addQuote = (
      <Fragment>
        <p className='Title'>Add new quote</p>
        <div>
          <p style={{color: '#9d9d9d'}}>Category</p>
          <select value={this.state.category}
                  onChange={this.changeCategoryHandler}>
            <option value="Star Wars">Star Wars</option>
            <option value="Famous people">Famous people</option>
            <option value="Saying">Saying</option>
            <option value="Humour">Humour</option>
            <option value="Motivational">Motivational</option>
          </select>
        </div>
        <div>
          <p style={{color: '#9d9d9d'}}>Author</p>
          <input type="text"
                 value={this.state.author}
                 onChange={this.changeAuthorHandler}/>
        </div>
        <div>
          <p style={{color: '#9d9d9d'}}>Quote text</p>
          <textarea value={this.state.text}
                    onChange={this.changeTextHandler} cols="30" rows="10"/>
        </div>
        <Button clicked={() => this.saveQuote()}>Save</Button>
      </Fragment>
    );

    if (this.state.loading) {
      addQuote = <Spinner/>
    }

    return(
      <div className='AddNewQuote'>
        {addQuote}
      </div>
    );
  }
}

export default AddNewQuote;